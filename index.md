---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="BeefUp is a responsive jQuery accordion plugin">
<meta name="keywords" content="beefup, jquery, accordion, css, javascript, plugin, collapsible">
<link rel="stylesheet" href="dist/css/styles.css">
<link rel="stylesheet" href="dist/css/jquery.beefup.css">
<link href='https://fonts.googleapis.com/css?family=Roboto:300,700' rel='stylesheet'>
</head>

<body>

<main>
	<h3 id="open-single">Open single</h3>

	<p>
		A minimal nested Accordion
	</p>

	<article class="beefup example-opensingle is-open">
		<h4 class="beefup__head">
			Item 1
		</h4>

		<div class="beefup__body">
			<p>
			actual text written in hospital
			</p>
		</div>
	</article>

	<article class="beefup example-opensingle">
		<h4 class="beefup__head">
			Item 2
		</h4>

		<div class="beefup__body">
			<div class="mockup"></div>

			<article class="beefup example-opensingle is-open">
				<h4 class="beefup__head">
					Nested Item 1
				</h4>

				<div class="beefup__body">
					<div class="mockup"></div>
				</div>
			</article>

			<article class="beefup example-opensingle">
				<h4 class="beefup__head">
					Nested Item 2
				</h4>

				<div class="beefup__body">
					<div class="mockup"></div>
				</div>
			</article>

			<article class="beefup example-opensingle is-closed">
				<h4 class="beefup__head">
					Nested Item 3
				</h4>

				<div class="beefup__body">
					<div class="mockup"></div>
				</div>
			</article>
		</div>
	</article>

	<p>
		Similarly but with info from _data
	</p>

<ul>
{% for f in site.data.foo %}
  <li>
			{{ f.title }}
  </li>
{% endfor %}
</ul>

	<p>
{{ site.data.foo[0].title }}
	</p>

	<article class="beefup example-opensingle is-open">
		<h4 class="beefup__head">
			{{ site.data.catA.foo[0].title }}
		</h4>

		<div class="beefup__body">
			<p>
			{{ site.data.catA.foo[0].description }}
			</p>
		</div>
	</article>

	<article class="beefup example-opensingle">
		<h4 class="beefup__head">
			{{ site.data.catB.foo[0].title }}
		</h4>

		<div class="beefup__body">
			{{ site.data.catB.foo[0].description }}

			<article class="beefup example-opensingle is-open">
				<h4 class="beefup__head">
					{{ site.data.catB.Bsuba.foo[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.catB.Bsuba.foo[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle">
				<h4 class="beefup__head">
					{{ site.data.catB.Bsubb.foo[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.catB.Bsubb.foo[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle is-closed">
				<h4 class="beefup__head">
					{{ site.data.catB.Bsubc.foo[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.catB.Bsubc.foo[0].description }}
				</div>
			</article>
		</div>
	</article>
</main>

<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/jquery.beefup.min.js"></script>
<script>
	$(function() {

		// Default
		$('.example').beefup();

		// Open single
		$('.example-opensingle').beefup({
			openSingle: true,
			stayOpen: 'last'
		});

		// Fade animation
		$('.example-fade').beefup({
			animation: 'fade',
			openSpeed: 400,
			closeSpeed: 400
		});

		// Scroll
		$('.example-scroll').beefup({
			scroll: true,
			scrollOffset: -10
		});

		// Self block
		$('.example-selfblock').beefup({
			selfBlock: true
		});

		// Self close
		$('.example-selfclose').beefup({
			selfClose: true
		});

		// Breakpoints
		$('.example-breakpoints').beefup({
			scroll: true,
			scrollOffset: -10,
			breakpoints: [
				{
					breakpoint: 768,
					settings: {
						animation: 'fade',
						scroll: false
					}
				},
				{
					breakpoint: 1024,
					settings: {
						animation: 'slide',
						openSpeed: 800,
						openSingle: true
					}
				}
			]
		});

		// API Methods
		var $beefup = $('.example-api').beefup();
		$beefup.open($('#beefup'));

		// Callback
		$('.example-callbacks').beefup({
			onInit: function($el) {
				$el.css('border-color', 'blue');
			},
			onOpen: function($el) {
				$el.css('border-color', 'green');
			},
			onClose: function($el) {
				$el.css('border-color', 'red');
			}
		});

		// Use HTML5 data attributes
		$('.example-data').beefup();

		// Tabs
		$('.tab__item').beefup({
			animation: '',
			openSingle: true,
			openSpeed: 0,
			closeSpeed: 0,
			onOpen: function($el) {
				// Add active class to tabs
				$('a[href="#' + $el.attr('id') + '"]').parent().addClass(this.openClass)
						.siblings().removeClass(this.openClass);
			}
		});

		// Dropdown
		var $dropdown = $('.dropdown').beefup({
			animation: 'fade',
			openSingle: true,
			selfClose: true
		});

		// Close dropdown
		$('.dropdown').on('click', 'li', function() {
			$dropdown.close();
		});
	});
</script>


